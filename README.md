Focus-pictures project

# Charte graphique

- Noir : #1D1D1D
- Blanc : #E2E2E2
- Bleus :
  - Foncé : #09273B
  - Moyen : #13547F
  - Clair : #1D81C5

# Template / Outil de style
https://bootswatch.com/lux/

# Message de présentation

## FR
Hello ! Moi, c'est Alexandre, je suis photographe de mariage, lifestyle et famille en dans toute la France et en Europe.
Depuis que je suis enfant, j'ai toujours été attiré par les technologies dont la photographie. Après des études d'ingénieur en système d'information et plusieurs mariages réalisés pour ma famille et des amis, j'ai décidé de créer FOCUS pictures, mon studio de photographie.
C'est avec plaisir que je vous accompagnerai lors de votre mariage ou au travers des séances photos. Je peux me déplacer dans toutes les régions de France et en Europe.

## EN
Hello ! I'm Alexandre, a photographer specialized in wedding, lifestyle and family in France and Europe.
Since I'm a child, I have always been attracted by technologies including photography. After an IT engineer degree and multiple weddings realized for my family and friends, I decided to create FOCUS pictures, my photography studio.
It woould be a pleasure to accompany you during your wedding or through a shooting. I can move all around France and in Europe.