import React, { useEffect } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import NotFound from '../component/notfound';
import Home from "../component/home/home";

interface Props {
    section: number,
    updateNavBarDisplay(isVisible : boolean) : any,
    sectionCall: number,
    collapsed: boolean,
    portraitImageLeftSize:boolean,
    movePortraitImage(left : boolean): any,
    changeSection(section:number):any,
    displayFront: boolean,
    wait(side:boolean, time:number):any,
    changeCollapsed(collapsed:boolean):any
}
const Router = ({ changeCollapsed, displayFront, wait, changeSection, movePortraitImage, collapsed, section, updateNavBarDisplay, sectionCall, portraitImageLeftSize }: Props) => {

    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" render={() => <Home changeCollapsed={changeCollapsed} wait={wait} displayFront={displayFront} changeSection={changeSection} movePortraitImage={movePortraitImage} portraitImageLeftSize={portraitImageLeftSize} collapsed={collapsed} section={section} updateNavBarDisplay={updateNavBarDisplay} sectionCall={sectionCall}/>} />
                <Route path="*" component={NotFound} />
            </Switch>
        </BrowserRouter>
    );
}
export default Router;