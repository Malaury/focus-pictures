import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Header from './component/header/header';
import * as serviceWorker from './serviceWorker';
import "bootswatch/dist/lux/bootstrap.min.css";
import '@blueprintjs/core/lib/css/blueprint.css';
import { Helmet } from 'react-helmet'

class Structure extends React.Component {

  render() {
    return (
      <div className="structure">
        <Helmet>
          <title>FOCUS pictures</title>
        </Helmet>
        <Header />
      </div>
    );
  }
}

ReactDOM.render(
  <Structure />,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

