import React, { useContext, useState, useEffect } from 'react';
import text from '../../utils/textTransleted';
import LanguageContext from '../../utils/MultilanguageContext';
import './portrait.css';
import { Form, FormGroup, Input } from 'reactstrap';
import '../../style/black_special_button.css';
import * as emailjs from 'emailjs-com';
import { Alert } from 'reactstrap';

interface Props {
    portraitImageLeftSize: boolean,
    movePortraitImage(left: boolean): any,
    sectionRef3: any,
    sectionRef4: any,
    displayFront: boolean,
    wait(side: boolean, time: number): any,
    changeSection(section:number):any,
    changeCollapsed(collapsed: boolean):any,
    collapsed: boolean
}
const Portrait = ({  collapsed, changeCollapsed, changeSection, displayFront, wait, sectionRef3, sectionRef4, portraitImageLeftSize, movePortraitImage }: Props) => {

    const { language } = useContext(LanguageContext);
    const [userMessage, setUserMessage] = useState({
        fullname: "",
        firstname: "",
        mail: "",
        category: "Mariage",
        message: ""
    });

    const [checkedForm, setCheckedForm] = useState(false);
    const [displayAlertMessage, setDisplayAlertMessage] = useState(false);

    function userMessageChange(event: any) {
        setUserMessage({
            ...userMessage,
            [event.target.name]: event.target.value
        });
    }

    const alex = '/assets/img/portrait/alex.JPG';
    const back = '/assets/img/portrait/back.png';

    function resetForm() {
        setUserMessage({
            fullname: "",
            firstname: "",
            mail: "",
            category: "Mariage",
            message: ""
        });
    }

    function handleSubmit() {
        changeCollapsed(true);
        if (userMessage.category === "" || userMessage.firstname === "" || userMessage.fullname === "" || userMessage.mail === "" || userMessage.message === "") {
            setCheckedForm(true);
        }
        else {
            let templateParams = {
                mail: userMessage.mail,
                firstname: userMessage.firstname,
                fullname: userMessage.fullname,
                category: userMessage.category,
                message: userMessage.message,
            }
            emailjs.send(
                'gmail_perso_',
                'mail',
                templateParams,
                'user_URD8Hf5ptm0IWEqxi662I'
            )
            resetForm();
            setDisplayAlertMessage(true);
            displayALert();
            setCheckedForm(false);
        }
    }

    function displayALert() {
        setTimeout(() => {
            setDisplayAlertMessage(false);
        }, 7000);
    }

    return (
        <>
            {window.innerWidth > 1215 ?
                <>
                    <div className={portraitImageLeftSize ? "image_portrait_right" : "image_portrait_left"} style={{ backgroundImage: "url(/assets/img/portrait/alex.JPG)" }}></div>
                    <div className="text-align-right">
                        <input src={back} type="image" className={displayFront ? "icon_back" : "icon_back front"} onClick={() => { movePortraitImage(true); wait(true, 100); changeCollapsed(true); }} />
                    </div>
                </>
                :
                <div className="custom-container">
                    <div className="col-12 no-padding-left no-padding-right">
                        <img src={alex} alt="alex_groux" className="image_alex"></img>
                    </div>
                </div>
            }
            <div ref={sectionRef3} className="container custom_container margin_top_portrait">
                <div className="row">
                    <div className={window.innerWidth > 1215 ? "col-xl-4 offset-xl-1 col-9 margin-left_portrait " : "col-xl-4 offset-xl-1 col-9 margin-left_portrait_mobile margin_top_white_description"}>
                        <h1 className={window.innerWidth > 1215 ? "" : "margin_white_description_mobile"}>{text[language].my_portrait}</h1>
                        <div className="custom_row">
                            <p className="p_classic p_portrait">{text[language].my_portrait_description_l1}</p>
                            <p className="p_bold p_portrait">{text[language].my_portrait_description_l2}</p>
                            <p className="p_classic p_portrait">{text[language].my_portrait_description_l3}</p>
                        </div>
                        <p className="p_classic p_portrait">{text[language].my_portrait_description_l4}</p>
                        <p className="p_classic p_portrait">{text[language].my_portrait_description_l5}</p>
                        <p className="p_classic p_portrait">{text[language].my_portrait_description_l6}</p>
                        <p className="p_classic p_portrait">{text[language].my_portrait_description_l7}</p>
                        <p className="p_classic p_portrait">{text[language].my_portrait_description_l8}</p>
                        <div className="col-4 no-padding-left">
                            <a target="_blank" href="https://www.facebook.com/FOCUS-pictures-111568893697276/"><input type="image" className="icon_portrait" src="/assets/icon/facebook_black.png" /></a>
                            <a target="_blank" href="https://www.instagram.com/_focus.pictures_/" ><input type="image" className="icon_portrait" src="/assets/icon/instagram_black.png" /></a>
                        </div>
                        <div className="col-4 no-padding-left">
                            <button className="special_button contact_me" onClick={() => { 
                                changeCollapsed(true);
                                wait(false, 1000); 
                                movePortraitImage(false);  
                                changeSection(4);
                            }}>{text[language].contact_me}</button>
                        </div>
                    </div>
                    <div ref={sectionRef4} className={window.innerWidth > 1215 ? "col-xl-5 offset-lg-2 margin_left_form margin_top_form" : "col-xl-5 offset-lg-2 margin_left_form margin_top_form margin_bot_form"}>
                        <div className="col-10 text-align">
                            <h1 className={window.innerWidth > 1215 ? "" : "margin_top_white_form_mobile"}> {text[language].contact_me} </h1>
                        </div>
                        <Form className="form_portrait">
                            <FormGroup>
                                <div className={!displayFront && !collapsed ? "custom_row front position_relative" : "custom_row"}>
                                    <div className="col-6 input_margin">
                                        <Input value={userMessage.fullname} onClick={() => changeCollapsed(true)} className={userMessage.fullname === "" && checkedForm === true ? "form-control input_style border-red" : "form-control input_style"} name="fullname" onChange={userMessageChange} placeholder={text[language].fullname} />
                                    </div>
                                    <div className="col-6 input_margin no-padding-left">
                                        <Input value={userMessage.firstname} onClick={() => changeCollapsed(true)} className={userMessage.firstname === "" && checkedForm === true ? "form-control input_style border-red" : "form-control input_style"} name="firstname" onChange={userMessageChange} placeholder={text[language].firstname} />
                                    </div>
                                </div>
                                <div className="col-12 input_margin">
                                    <Input value={userMessage.mail} onClick={() => changeCollapsed(true)} type="email" className={userMessage.mail === "" && checkedForm === true ? "form-control input_style border-red" : "form-control input_style"} name="mail" onChange={userMessageChange} placeholder={text[language].mail} />
                                </div>
                                <div className="col-12 input_margin">
                                    <Input type="select" onClick={() => changeCollapsed(true)} value={userMessage.category} className="form-control input_style" name="category" onChange={userMessageChange} placeholder={text[language].category}>
                                        <option value={text[language].wedding}>{text[language].wedding}</option>
                                        <option value={text[language].family}>{text[language].family}</option>
                                        <option value={text[language].portrait}>{text[language].portrait}</option>
                                        <option value={text[language].other}>{text[language].other}</option>
                                    </Input>
                                </div>
                                <div className="col-12 input_margin">
                                    <Input type="textarea" onClick={() => changeCollapsed(true)} value={userMessage.message} className={userMessage.message === "" && checkedForm === true ? "input_style border-red" : "input_style"} name="message" onChange={userMessageChange} placeholder={text[language].message} />
                                </div>
                            </FormGroup>
                        </Form>
                            <div className="col-12 text-align-right">
                                <p className="mandatory_champs">{text[language].mandatoryField}</p>
                                <div className="row">
                                    <div className="col-6 position_relative text-align">
                                        {displayAlertMessage ?<Alert color="info alert info-custom">
                                        {text[language].successSend}
                                    </Alert>:null}
                                    </div>
                                    <button className="col-4 special_button" onClick={() => { handleSubmit(); }}>{text[language].send}</button>
                                </div>
                            </div> 
                    </div>
                </div>
            </div>
        </>
    );
}
export default Portrait;