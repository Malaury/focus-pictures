import Carrousel from '../carrousel/carrousel';
import React, { useRef, useEffect, useState } from 'react';
import Portrait from '../portrait/portrait';
import './home.css';
import VisibilitySensor from "react-visibility-sensor";

interface Props {
    section: number,
    updateNavBarDisplay(isVisible: boolean): any,
    sectionCall: number,
    collapsed: boolean,
    portraitImageLeftSize:boolean,
    movePortraitImage(left: boolean): any,
    changeSection(section:number):any,
    displayFront: boolean,
    wait(side:boolean,time:number):any,
    changeCollapsed(collapsed:boolean):any
}
const Home = ({ changeCollapsed, displayFront, wait, changeSection, movePortraitImage, collapsed, section, updateNavBarDisplay, sectionCall, portraitImageLeftSize }: Props) => {

    const sectionRef1: any = useRef(React.createRef());
    const sectionRef2: any = useRef(React.createRef());
    const sectionRef3: any = useRef(React.createRef());;
    const sectionRef4: any = useRef(React.createRef());;

    const scroll = (ref: any) => {
        ref.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });
    };

    function scrollToSection(section: number) {
        if (section === 1) {
            scroll(sectionRef1);
        }
        else if (section === 2) {
            scroll(sectionRef2);
        }
        else if (section === 3) {
            scroll(sectionRef3);
        }
        else if (section === 4) {
            scroll(sectionRef4);
        }
    }

    function onChange(isVisible: boolean) {
        updateNavBarDisplay(isVisible);
    }

    useEffect(() => {
        scrollToSection(section);
    }, [sectionCall]);

    return (
        <div>
            <div ref={sectionRef1} />
            <div >
                <VisibilitySensor partialVisibility={true} onChange={onChange}>
                    <Carrousel changeCollapsed={changeCollapsed} collapsed={collapsed} changeSection={changeSection}></Carrousel>
                </VisibilitySensor>
            </div>
            <div ref={sectionRef2} />
            <Portrait collapsed={collapsed} changeCollapsed={changeCollapsed} changeSection={changeSection} wait={wait} displayFront={displayFront} sectionRef3={sectionRef3} sectionRef4={sectionRef4} portraitImageLeftSize={portraitImageLeftSize}  movePortraitImage={movePortraitImage}></Portrait>
        </div>
    );
}
export default Home;