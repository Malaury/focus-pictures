import * as React from "react";
import { useState, useContext } from "react";
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import text from '../../utils/textTransleted';
import LanguageContext from '../../utils/MultilanguageContext';
import './multilanguages.css';

export interface MultilanguageProps {
    compiler: string;
    framewoork: string
}

const Multilanguage = () => {

    const [dropdownOpen, setDropdownOpen] = useState(false);

    const {language, updateLanguage} = useContext(LanguageContext);

    const toggle = () => setDropdownOpen(prevState => !prevState);

    function changeLanguage(languageCode: String): void{
        let languageSelected=0;
        switch (languageCode){
            case "fr":
                languageSelected = 0;
                break;
            case "en":
                languageSelected = 1;
                break;
        }
        updateLanguage(languageSelected);
    }

    return (
        <Dropdown className="btn" isOpen={dropdownOpen} toggle={toggle}>
            <DropdownToggle caret>
                {text[language].acronym}
        </DropdownToggle>
            <DropdownMenu>
                <DropdownItem onClick={() => {changeLanguage("fr")}} >{text[language].french}</DropdownItem>
                <DropdownItem onClick={() => {changeLanguage("en")}} >{text[language].english}</DropdownItem>
            </DropdownMenu>
        </Dropdown>
    );
}
export default Multilanguage;