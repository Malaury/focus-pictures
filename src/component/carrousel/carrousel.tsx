import React, { useState, useContext, useEffect } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';
import './carrousel.css';
import text from '../../utils/textTransleted';
import LanguageContext from '../../utils/MultilanguageContext';
import '../../style/white_special_button.css';

let image1 = new Image();
image1.src='/assets/img/carousel/boreal.jpg';

let image2 = new Image();
image2.src='/assets/img/carousel/elan.jpg';

let image3 = new Image();
image3.src='assets/img/carousel/amandine.jpg';

const items = [
  image1,
  image2,
  image3
];
interface Props {
  collapsed: boolean,
  changeSection(section: number): any,
  changeCollapsed(collapsed: boolean): any
}
const CarouselFP = ({ changeCollapsed, collapsed, changeSection }: Props) => {
  const { language } = useContext(LanguageContext);
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);


  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
        className="text_hover"
      >
        <div style={{ backgroundImage: "url("+item.src+")" }} className="img carrousel-img" />
      </CarouselItem>
    );
  });

  const carouselPhrases = () => {
    return (
      <>
        <div className="col-12 absolute_name text-align">
          <p className="name">ALEXANDRE GROUX</p>
        </div>
        <div className="col-12 absolute_text text-align">
          <p className="text_description">{text[language].text_carousel}</p>
        </div>
        <div className="col-12 absolute_button text-align">
          <button className="white_special_button" type="submit" onClick={() => {
            changeCollapsed(true);
            if (window.innerWidth > 1215) {
              changeSection(2);
            }
            else {
              changeSection(3);
            }
          }}>{text[language].my_portrait}</button>
        </div>
      </>
    );
  }

  return (
    <div className="container custom_container no-padding-left no-padding-right">
      <div className="row position_relative no_margin">
        <div className="col-12 no_padding">
          <Carousel activeIndex={activeIndex} next={next} previous={previous} className="text_hover">
            {slides}
            {window.innerWidth < 600 && collapsed ?
              <>

                <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
                <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
                <div className="col-12 absolute_button text-align">
                  <button className="white_special_button button_view_mobile" type="submit"
                    onClick={() => {
                      changeCollapsed(true);
                      if (window.innerWidth > 1215) {
                        changeSection(2);
                      }
                      else {
                        changeSection(3);
                      }
                    }}>{text[language].my_portrait}</button>
                </div>
              </> : null}
          </Carousel>
        </div>
        {window.innerWidth > 575 ?
          <> {window.innerWidth > 715 || collapsed ? carouselPhrases() : null}</> : null}
      </div>
    </div>
  );
}
export default CarouselFP;