import React from 'react';

const NotFound = () => {
    return(
        <h4>Error 404 - Ressource not found :)</h4>
    );
}
export default NotFound;