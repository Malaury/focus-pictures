import React, { useState, useEffect, useCallback } from 'react';
import './header.css';
import Router from '../../utils/router';
import Multilanguage from '../multilanguages/multilanguages';
import LanguageContext from '../../utils/MultilanguageContext';
import text from '../../utils/textTransleted';
import { Drawer, Navbar, NavbarGroup, NavbarHeading, Alignment } from '@blueprintjs/core';

const Header = () => {

    const [collapsed, setCollapsed] = useState(true);
    const [section, setSection] = useState(0);
    const [sectionCall, setSectionCall] = useState(0);
    const [portraitVisible, setPortraitVisible] = useState(false);
    const [language, updateLanguage] = useState(0);
    const [windowSize, setWindowSize] = useState(window.innerWidth);
    const [portraitImageLeftSize, setPortraitImageLeftSize] = useState(true);
    const [displayFront, setDisplayFront] = useState(true);

    function wait(side: boolean, time: number){
        setTimeout(() => {
            setDisplayFront(side);
          }, time);
    }

    function changeCollapsed(collapsed: boolean){
        setCollapsed(collapsed);
    }


    const handleWindowResize = useCallback(event => {

        setWindowSize(window.innerWidth);

    }, []);

    function movePortraitImage(left: boolean){
        setPortraitImageLeftSize(left);
    }

    useEffect(() => {
        window.addEventListener('resize', handleWindowResize);

        return () => {
            window.removeEventListener('resize', handleWindowResize);
        };
    }, [handleWindowResize]);

    const languageValue = {
        language: language,
        updateLanguage: updateLanguage
    }

    function updateNavBarDisplay(isVisible: boolean) {
        setPortraitVisible(isVisible);
    }

    const logo_white = '/assets/img/logo/logo_white.png';

    const toggleNavbar = () => {
        setCollapsed(!collapsed);
    }

    function changeSection(section: number) {
        setCollapsed(true);
        setSection(section);
        setSectionCall(sectionCall + 1);
    }

    return (
        <LanguageContext.Provider value={languageValue}>
            <Navbar fixedToTop className={portraitVisible === true ? "" : window.innerWidth>575 ? "navbar_scrolled navbar_scrolled_desktop" :"navbar_scrolled navbar_scrolled_mobile"}>
                <NavbarGroup align={Alignment.LEFT}>
                    <div className="container custom_container no_padding">
                        <div className="row">
                            <div className="col-4 col-sm-2">
                                <NavbarHeading>{collapsed ? <input className="menu_icon_tooggle_close" type="image" onClick={toggleNavbar} src="/assets/icon/menu.png" /> : null} </NavbarHeading>
                            </div>
                            {windowSize > 575 ?
                                <>
                                    {windowSize > 1015 || collapsed ?
                                        <>
                                            <div className="col-4 col-sm-8 text-align">
                                                <img className="img-logo" src={logo_white} />
                                            </div>
                                            <div className="col-4 col-sm-2 text-align">
                                                <Multilanguage></Multilanguage>
                                            </div>
                                        </> :
                                        <>
                                            <div className="col-8 col-sm-10 text-align-right">
                                                <div className="multilangue_collapsed_view_less_than_1000">
                                                    <Multilanguage></Multilanguage>
                                                </div>
                                            </div>
                                        </>
                                    }
                                </>
                                : <>
                                    {collapsed ?
                                        <div className="col-8 text-align-right">
                                             <img className="img-logo_mobile" src={logo_white} />
                                        </div> : null
                                    }
                                </>
                            }
                        </div>
                    </div>
                </NavbarGroup>
            </Navbar>
            <div className="container custom_container no_padding">
                <Drawer isOpen={!collapsed} className={portraitVisible === true ? "" : "bp3-navbar-scrolled"} position='left'>
                    <div className="row">
                        <div className="col-12 text-align-right">
                            <input className="menu_icon_close" type="image" onClick={toggleNavbar} src="/assets/icon/close.png" />
                        </div>
                    </div>
                    <ul>
                        <li className="navbar_item_title" onClick={() => changeSection(1)}>{text[language].home} </li>
                        <li className="navbar_item_title" onClick={() => {
                            if(window.innerWidth >1215){
                                changeSection(2); 
                                setPortraitImageLeftSize(true);
                                wait(true,100);
                            }
                            else{
                                changeSection(3);
                            } 
                        }}>{text[language].my_portrait} </li>
                        <li className="navbar_item_title" onClick={() => {
                            if(window.innerWidth > 1215){
                                changeSection(2); 
                                setPortraitImageLeftSize(false);
                                wait(false,1000);
                            }
                            else{
                                changeSection(4);
                            }
                        }}>{text[language].contact_me}</li>
                    </ul>
                    <div className="row">
                        <div className="col-12 text-align-right icon-placement">
                            <a href="https://www.facebook.com/FOCUS-pictures-111568893697276/" target="_blank"><input type="image" className="icon-rsx facebook-icon" src="/assets/icon/facebook.png"/></a>
                            <a href="https://www.instagram.com/_focus.pictures_/" target="_blank" ><input type="image" className="icon-rsx instagram-icon" src="/assets/icon/instagram.png" /></a>
                        </div>
                    </div>
                </Drawer>
            </div>
            <Router changeCollapsed={changeCollapsed} wait={wait} displayFront={displayFront} portraitImageLeftSize={portraitImageLeftSize} movePortraitImage={movePortraitImage} section={section} collapsed={collapsed} updateNavBarDisplay={updateNavBarDisplay} sectionCall={sectionCall}  changeSection={changeSection}></Router>
        </LanguageContext.Provider >
    );
}
export default Header;